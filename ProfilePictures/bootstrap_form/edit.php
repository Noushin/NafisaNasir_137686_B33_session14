<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../assest/bootstrap/css/bootstrap.min.css">
    <script src="../../assest/bootstrap/js/jquery.min.js"></script>
    <script src="../../assest/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Profile Picture Create Form</h2>
    <form>
        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" class="form-control" id="name" placeholder="Enter name">
        </div>
        <div class="form-group">
            <label for="Profile Picture">Profile Picture:</label>
            <input type="text" class="form-control" id="Profile Picture" placeholder="Enter Profile Picture">
        </div>
        <div class="checkbox">
            <label><input type="checkbox"> Remember me</label>
        </div>
        <button type="submit" class="btn btn-danger">Submit</button>
        <button type="submit" class="btn btn-danger">RESET</button>
        <button type="submit" class="btn btn-danger">delete</button>
    </form>
</div>

</body>
</html>